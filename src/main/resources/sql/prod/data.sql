USE dwfe_prod;

LOCK TABLES
dwfe_countries WRITE,
dwfe_nevis_authorities WRITE;

--
-- DWFE
--

INSERT INTO dwfe_countries
VALUES ('Russia', 'RU', 'RUS', '7'),
       ('Ukraine', 'UA', 'UKR', '380'),
       ('Germany', 'DE', 'DEU', '49'),
       ('United States', 'US', 'USA', '1'),
       ('United Kingdom', 'GB', 'GBR', '44'),
       ('Japan', 'JP', 'JPN', '81');

--
-- MODULE: Nevis
--

INSERT INTO dwfe_nevis_authorities
VALUES ('NEVIS_ADMIN', 'Administrator of Nevis'),
       ('NEVIS_USER', 'Standard Nevis User'),
       ('BALI_ADMIN', 'Administrator of Bali'),
       ('BALI_USER', 'Standard Bali User'),
       ('STORAGE_ADMIN', 'Administrator of Storage'),
       ('STORAGE_USER', 'Standard Storage User');


UNLOCK TABLES;
