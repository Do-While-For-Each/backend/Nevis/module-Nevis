package dwfe.modules.nevis.schedule;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.nevis.config.NevisConfigProperties;
import dwfe.modules.nevis.db.mailing.NevisMailing;
import dwfe.modules.nevis.db.mailing.NevisMailingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;

import static dwfe.modules.nevis.db.mailing.NevisMailingType.EMAIL_CONFIRM;
import static dwfe.modules.nevis.db.mailing.NevisMailingType.PASSWORD_RESET_CONFIRM;
import static dwfe.util.DwfeUtil.*;

@Component
public class NevisScheduler
{
  private final static Logger log = LoggerFactory.getLogger(NevisScheduler.class);

  private final DwfeConfigProperties propDwfe;

  private final RestTemplate ribbonRestTemplate;

  private final NevisMailingService mailingService;
  private static final ConcurrentSkipListSet<NevisMailing> MAILING_POOL = new ConcurrentSkipListSet<>();
  private final int maxAttemptsMailingIfError;

  private final String EXTERNAL_MAILING_URI;

  @Autowired
  public NevisScheduler(DwfeConfigProperties propDwfe,
                        NevisConfigProperties propNevis,
                        NevisMailingService mailingService,
                        @Qualifier("RibbonRestTemplate") RestTemplate ribbonRestTemplate)
  {
    this.propDwfe = propDwfe;
    this.mailingService = mailingService;
    this.maxAttemptsMailingIfError = propNevis.getScheduledTaskMailing().getMaxAttemptsToSendIfError();
    this.ribbonRestTemplate = ribbonRestTemplate;

    this.EXTERNAL_MAILING_URI = String.format("http://%s/personal", propDwfe.getService().getMailing().getName());
  }


  @Scheduled(
          initialDelayString = "#{nevisConfigProperties.scheduledTaskMailing.initialDelay}",
          fixedRateString = "#{nevisConfigProperties.scheduledTaskMailing.collectFromDbInterval}")
  public void collectMailingTasksFromDatabase()
  {
    MAILING_POOL.addAll(mailingService.getNewJob());
    log.debug("mailing [{}] collected from DB", MAILING_POOL.size());
  }


  @Scheduled(
          initialDelayString = "#{nevisConfigProperties.scheduledTaskMailing.initialDelay}",
          fixedDelayString = "#{nevisConfigProperties.scheduledTaskMailing.sendInterval}")
  public void sendingMail()
  {
    log.debug("mailing [{}] before sending", MAILING_POOL.size());
    final var toDataBase = new ArrayList<NevisMailing>();
    MAILING_POOL.forEach(next -> {
      var type = next.getType();
      var email = next.getEmail();
      var data = next.getData();
      try
      {
        var req = RequestEntity
                .post(URI.create(EXTERNAL_MAILING_URI))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(getJsonFromObj(Map.of(
                        "module", propDwfe.getAppName(),
                        "type", type,
                        "email", email,
                        "data", data
                )));
        var resp = ribbonRestTemplate.exchange(req, new ParameterizedTypeReference<Map<String, Object>>()
        {
        });
        var body = resp.getBody();
        if (isReqSuccess(body))
        {
          next.setScheduled(true);
          clearMailing(next);
          toDataBase.add(next);
          log.debug("mailing <{}> successfully scheduled, {}", email, type);
        }
        else
        {
          failedSendHandler(next, getErrorCode(body), toDataBase);
        }
      }
      catch (Throwable e)
      {
        failedSendHandler(next, e.toString(), toDataBase);
      }
    });

    if (toDataBase.size() > 0)
    {
      mailingService.saveAll(toDataBase);
      MAILING_POOL.removeAll(toDataBase);
      log.debug("mailing [{}] stored to DB", toDataBase.size());
      toDataBase.clear();
    }
  }

  private void clearMailing(NevisMailing mailing)
  {
    var type = mailing.getType();

    // if not confirmation
    if (!(EMAIL_CONFIRM.equals(type) || PASSWORD_RESET_CONFIRM.equals(type)))
      mailing.clear();
  }

  private void failedSendHandler(NevisMailing mailing, String cause, List<NevisMailing> toDataBase)
  {
    var type = mailing.getType();
    var email = mailing.getEmail();

    mailing.setCauseOfLastFailure(cause);

    if (mailing.getAttempt().incrementAndGet() > maxAttemptsMailingIfError)
    {
      mailing.setMaxAttemptsReached(true);
      clearMailing(mailing); // but all of a sudden the letter was scheduled
      toDataBase.add(mailing);
      log.debug("mailing <{}> last fail scheduling, {}: {}", email, type, cause);
    }
    else log.debug("mailing <{}> go to attempt[{}] after fail, {}: {}", email, mailing.getAttempt().get(), type, cause);
  }
}
